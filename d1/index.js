console.log('Hello World');

// JSON Object
// Javascript Object Notation
// is is a data format

// JSON is used for serializing different data types into bytes.

/*
	Syntax:
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}
*/

// sample1

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON Arrays
// sample2
// {
// 	"cities": [
// 		{"city": "QC", "province": "Manila", "country": "Philippines"}
// 		{"city": "Manila", "province": "Manila", "country": "Philippines"}
// 		{"city": "Makati", "province": "Manila", "country": "Philippines"}
// 	]
// }

// JSON Methods

// Convert Data into Stingified JSON (cliet to server / frontend to backend)

let batchesArr = [
	
	{batchName: 'Batch X'},
	{batchName: 'Batch Y'},
]
console.log('Result from array of objects')
console.log(batchesArr)	;//OBJECT sample (server to client)


// before sending data, use strigify to covert Objects into JSON.
console.log('Result from stringify method');
console.log(JSON.stringify(batchesArr));//STRINGIFY sample (client to server)

let data =JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'PH'
	}
});

console.log(data);


// sample 3 (client to server)

let firstName = prompt('What is your first name?')
let lastName = prompt('What is your last name?');
let age = prompt('What is your age');
let address = {
	city: prompt('which city do you live in?'),
	country: prompt('Which country does your city belong to')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherData);

// Converting Stringified JSON into javascript objects
// upon recieving data, JSON text can be converted to a JS objects with parse method

// JSON.parse()
// sample1 (server to client)

let batchesJSON = `[
	{"batchName": "Batch X"},
	{"batchName": "Batch Y"}
]`

console.log(batchesJSON);

console.log('Result from parse to method');
console.log(JSON.parse(batchesJSON));


// Mini Activity

